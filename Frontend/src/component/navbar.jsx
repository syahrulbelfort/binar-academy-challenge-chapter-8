import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';

function BrandExample() {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="#home">
            <img
              alt=""
              src="http://localhost:3000/logo192.png"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{' '}
            React Form Practice
          </Navbar.Brand>
        </Container>
      </Navbar>
    </>
  );
}

export default BrandExample;