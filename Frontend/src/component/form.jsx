import React, { useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import CSS from '../../src/form.module.css';
import TableData from './tableData'

function NewPlayerForm() {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [experience, setExperience] = useState('');
  const [tableData, setTableData] = useState([]);
  const [editIndex, setEditIndex] = useState(null);
  const [searchTerm, setSearchTerm]= useState('');

  const handleSearch = (e) =>{
    setSearchTerm(e.target.value)
  };

 const filterData = tableData.filter((player)=>{
  return(
    player.username.toLowerCase().includes(searchTerm.toLowerCase()) ||
    player.email.toLowerCase().includes(searchTerm.toLowerCase())
  )
 })

  const handleChange = (event) => {
    event.preventDefault();
    const newPlayer = {
      username,
      email,
      password,
      experience
    };

    editIndex === null ? setTableData([...tableData, newPlayer]) : (()=>{
      const newTableData = [...tableData];
      newTableData[editIndex] = newPlayer;
      setTableData(newTableData);
      setEditIndex(null)
    })();
    setUsername('');
    setEmail('');
    setPassword('');
    setExperience('');
  };

  const handleEdit = (index) => {
    setEditIndex(index);
    setUsername(tableData[index].username);
    setEmail(tableData[index].email);
    setPassword(tableData[index].password);
    setExperience(tableData[index].experience);
  };

  return (
    <>
      <Container className='mt-3'>
        <h1>Register</h1>
        <hr/>
        <Form onSubmit={handleChange} className='d-flex'>
          <Form.Group className={CSS.formKartu} controlId="formPlayerName">
            <Form.Label>Username</Form.Label>
            <Form.Control type="text" onChange={(e)=>setUsername(e.target.value)} className={CSS.formKartu} placeholder="Enter username" value={username} />
          </Form.Group>
          <Form.Group controlId="formPlayerEmail" className={`${CSS.formKartu}ms-2 me-2`}>
            <Form.Label>Email</Form.Label> 
            <Form.Control type="email" placeholder="Masukkan email" value={email} onChange={(e) => setEmail(e.target.value)} />
          </Form.Group>
          <Form.Group controlId="formPlayerPassword" className='me-2'>
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" className={CSS.formKartu} placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} />
          </Form.Group>
          <Form.Group controlId="formPlayerExperience">
            <Form.Label>Experience</Form.Label>
            <Form.Control type="number" className={CSS.formKartu} placeholder="Enter experience" value={experience} onChange={(e) => setExperience(e.target.value)} />
          </Form.Group>
          <Button variant="primary" className={`${CSS.btnCustom}`} type="submit" style={{ marginTop: '1.5rem' }}>
            {editIndex === null ? 'Add' : "Update"}
          </Button>
        </Form>
        <label className='mt-3'>
        <h1>Search</h1>
        <input type="text" className={CSS.formKartu} placeholder="Search" value={searchTerm} onChange={handleSearch} />
        </label>
        <hr/>
        <TableData data={filterData} handleEdit={handleEdit}/>
      </Container>
    </>
  );
}

export default NewPlayerForm;
