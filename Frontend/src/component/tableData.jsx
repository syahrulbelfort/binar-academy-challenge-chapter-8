import React from 'react';
import { Table, Button } from 'react-bootstrap';

function TableComponent(props) {
  const { data, handleEdit } = props;

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Username</th>
          <th>Email</th>
          <th>Password</th>
          <th>Experience</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {data.map((player, index) => (
          <tr key={index}>
            <td>{player.username}</td>
            <td>{player.email}</td>
            <td>{player.password}</td>
            <td>{player.experience}</td>
            <td><Button onClick={()=> handleEdit(index)}>Edit </Button></td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}

export default TableComponent;
