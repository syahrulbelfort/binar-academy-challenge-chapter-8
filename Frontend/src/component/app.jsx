import React from "react";
import Form from './form'
import Navbar from './navbar'
export default function App(){
    return(
    <>
        <Navbar/>
        <Form/>
    </>)
}