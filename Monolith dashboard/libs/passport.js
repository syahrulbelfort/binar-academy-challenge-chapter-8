const passport = require('passport');
const localStrategy = require('passport-local');
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');
const { User_game } = require('../models');

// fungsi untuk authentication
async function authenticate(email, password, done) {
  try {
    // Memanggil method yang tadi
    const user = await User_game.authenticate({ email, password });
    return done(null, user);
  } catch (err) {
    return done(null, false, { message: 'Password salah!' });
  }
}

passport.use(new localStrategy({ usernameField: 'email', passwordField: 'password' }, authenticate));

//JWT Strategy
const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken('Authorization'),
  secretOrKey: 'ini rahasia, tidak boleh disebar',
};

passport.use(
  new JwtStrategy(options, async (payload, done) => {
    try {
      const user = await User_game.findByPk(payload.id);
      done(null, user);
    } catch (err) {
      done(err, false);
    }
  })
);
//END OF JWT STRATEGY

//serialize & deserialize cara membuat sesi dan menghapus sesi
passport.serializeUser((user, done) => done(null, user.id));
passport.deserializeUser(async (id, done) => done(null, await User_game.findByPk(id)));
//di eksport karena kita akan gunakan sebagai middleware

module.exports = passport;
