const { User } = require('../models');
const base_response = require('../libs/base-response');

const format = (user) => {
  const { id, email } = user;
  return {
    message: 'Anda berhsail login..',
    id,
    email,
    accesToken: user.generateToken(),
  };
};

module.exports = {
  login: async (req, res) => {
    try {
      const user = await User.authentication(req.body);
      res.json(format(user));
    } catch (err) {
      res.json({
        status: 403,
        message: 'login fail',
      });
    }
  },
  register: async (req, res, next) => {
    const { email, password, confirmationPassword } = req.body;

    if (password !== confirmationPassword) {
      res.status(400).json(base_response(null, 'failed', 'Password konfirmasi tidak sesuai!'));
    }

    try {
      const user = await User.registration({ email, password });
      res.status(200).json(base_response(user, 'success', 'Registrasi Berhasil!'));
    } catch (error) {
      res.status(400).json(base_response(null, 'failed', error));
    }
  },
  loginToken: async (req, res, next) => {
    const { email, password } = req.body;
    User.authenticateToken({ email, password })
      .then(async (user) => {
        const data = {
          id: user.id,
          username: user.email,
          accessToken: await User.generateTokenV2({ id: user.id, email: user.email }),
        };
        res.status(200).json(base_response(data, 'success', 'Login Berhasil!'));
      })
      .catch((err) => {
        res.status(401).json(base_response(null, 'failed', err));
      });
  },
};
