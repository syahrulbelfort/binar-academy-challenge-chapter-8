module.exports = {
  games: (req, res) => {
    res.render('pages/game/gameplay', req.user.dataValues);
  },
};
