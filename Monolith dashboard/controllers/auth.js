const { User_game } = require('../models');
const passport = require('passport');

module.exports = {
  loginShow: (req, res) => {
    res.render('pages/auth/login');
  },
  login: passport.authenticate('local', {
    successRedirect: '/games',
    failureRedirect: '/login',
    failureFlash: true,
  }),
  logout: (req, res, next) => {
    req.logout((err) => {
      if (err) {
        return next(err);
      }
      res.redirect('/login');
    });
  },
  registerShow: (req, res) => {
    res.render('pages/auth/register');
  },
  register: async (req, res, next) => {
    try {
      await User_game.register(req.body);
      res.redirect('/login');
    } catch (err) {
      next(err);
    }
  },
};
