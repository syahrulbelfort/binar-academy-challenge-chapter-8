const express = require('express');
const gameplay = require('../controllers/gameplay');
const game = express.Router();
const restrict = require('../middleware/restrict');

const layoutName = (name)=>{
    return(req,res,next)=>{
        res.locals.layout = `layouts/${name}`
        next()
    }
}

game.use(layoutName('game'))

game.get('/games', restrict, gameplay.games);

module.exports = game;
