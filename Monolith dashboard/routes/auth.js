const express = require('express');
const authJson = require('../controllers/authJson');
const authMvc = require('../controllers/auth');
const auth = express.Router();

const layoutName = (name)=>{
    return(req,res,next)=>{
        res.locals.layout = `layouts/${name}`
        next()
    }
}

auth.use(layoutName('auth'))
//register routes MVC
auth.get('/register', authMvc.registerShow);
auth.post('/register', authMvc.register);

//login routes MVC
auth.get('/login', authMvc.loginShow);
auth.post('/login', authMvc.login);

//register API MCR
auth.post('/registration', authJson.register);

//login for user not admin MCR
auth.post('/auth/token', authJson.loginToken);

//logout routes MVC
auth.post('/logout', authMvc.logout);

module.exports = auth;
