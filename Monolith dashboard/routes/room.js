const express = require('express');

const checkToken = require('../middleware/checkToken');
const rooms = require('../controllers/room');
const room = express.Router();

room.post('/v1/create-room', checkToken, rooms.createRoom);

room.post('/v1/room/join', checkToken, rooms.joinRoom);

room.post('/v1/fight/:id', checkToken, rooms.fight);

module.exports = room;
