const express = require('express');
const home = express.Router();
const homeRoute = require('../controllers/home');


const layoutName = (name)=>{
    return(req,res,next)=>{
        res.locals.layout = `layouts/${name}`
        next()
    }
}

home.use(layoutName('default'))
//home routes
home.get('/', homeRoute.index);

module.exports = home;
